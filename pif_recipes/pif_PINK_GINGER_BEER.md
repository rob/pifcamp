# pif_PINK_GINGER_BEER

### something clean and refreshing for junior piflars and anyone in need of alcohol and caffine free refreshment
Recipe makes 18L for carbonation and dispensing in a Cornelius keg.

### INGREDIENTS:
#### WATER
- 16.5L of Soca River Water.

#### GINGER
- 1kg of Fresh Ginger

#### GINGER BUG
- 5 days in advance of brewday, prepare a 2L ginger bug starter.

#### SWEETENER
- 1.2kg of cane sugar.

#### COLOURING
- A medium sized beetroot

#### ACIDITY
- 30g Citric Acid (Substitute with the juice of 12 Lemons juice and a little lemon zest if you have...)

## METHOD

- Boil water for 1 hour to annihilate any potential pathogens present in river water. Extra half liter for splash and spillage and evaporation during boil.

- After 30mins add the sliced ginger

- Drop to 65 Celsius and add sliced fresh beetroot and sugar.
// at this temperature we sanitise / pasturise the beetroot and sugar and get a good colour from the beetroot without too much earthy flavour.

- Rest for 10 minutes stirring gently to allow sugar to dissolve and a little colour to leech from beetroot.
- Remove ginger and beetroot

- Drop temperature to ~30C using a stainless steel coil immersion chiller.

- Transfer to a sanitised 18L Cornelius keg

// At this point you can either wait a few days and let natural fermentation run its course and have a very mildly alcoholic fizzy drink

&& OR

- Drop temperature as low as possible (immerse and leave keg in river?)

- Connect to a CO2 cylinder, purge air and force carbonate at 40 PSI / 3 Bar for 24 hours agitating regularly to assist CO2 absorption. If working on a smaller scale you could use a SodaStream machine to carbonate.

// Using this method you will have no alcohol. You could also skip the ginger bug step if 0% alochol is your desired outcome, but without forced carbonation with CO2 or fermentation you will not get a carbonated drink.

- Add citric acid || lemon juice and agitate the keg.

- Connect to chiller / dispensing tap and serve chilled.
