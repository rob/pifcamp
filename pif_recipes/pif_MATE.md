# pif_MATE

### a drink for the pifcamp piflars - inspired by club mate
Recipe makes 18L for carbonation and dispensing in a Cornelius keg.
### INGREDIENTS

#### WATER
- 18.5L of Soca River Water.

#### TEA
- 180g of Mate tea.

#### SWEETENER
- 500g of cane sugar.

#### BOTANICALS
- A large double handful of Achillea millefolium [SLO: Rman, ENG: Yarrow]

#### ACIDITY
- 30g Citric Acid (Substitute with the juice of 12 Lemons juice and a little lemon zest if you have...)

## METHOD

- Boil water for 1 hour to annihilate any potential pathogens present in river water. Extra half liter for tea absorption, splash and spillage and evaporation during boil.

- Allow temperature to drop to 92 Celsius.

- add Tea and Achillea millefolium - I used a large hop filter from a brew kettle, if added loose you will need a large stainless steel strainer to filter.

- Steep for 12 minutes

- Remove tea and yarrow.

- Drop temperature to ~30C using a stainless steel coil immersion chiller.

- Add citric acid or lemon juice.

- Transfer to a sanitised 18L Cornelius keg

- Drop temperature as low as possible (immerse and leave keg in river?)

- Connect to a CO2 cylinder, purge air and force carbonate at 40 PSI / 3 Bar for 24 hours agitating regularly to assist CO2 absorption. If working on a smaller scale you could use a SodaStream machine to carbonate.

- Connect to chiller / dispensing tap and serve chilled.
